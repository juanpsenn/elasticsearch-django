from django.shortcuts import render
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .models import *
from utils.functions import query_debugger

# Create your views here.


@api_view()
def list_products(request):
    iterate()
    return Response(status=200)


@query_debugger
def iterate():
    products = Product.objects.prefetch_related(
        'stock').select_related('brand')
    stocks = [[{'amount': s.amount, 'brand': product.brand.name}
               for s in product.stock.all()] for product in products]
    print(stocks)
