from django.db import models

# Create your models here.


class Product(models.Model):
    name = models.CharField(max_length=128)
    brand = models.ForeignKey('Brand', on_delete=models.DO_NOTHING, null=True)


class Brand(models.Model):
    name = models.CharField(max_length=128)


class Stock(models.Model):
    amount = models.IntegerField(default=0)
    product = models.ForeignKey(
        'Product', on_delete=models.DO_NOTHING, related_name='stock')
