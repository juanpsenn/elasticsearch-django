from django_elasticsearch_dsl import Document, fields
from django_elasticsearch_dsl.registries import registry
from .models import Product, Brand, Stock
from utils.functions import query_debugger


@registry.register_document
class ProductDocument(Document):
    brand = fields.NestedField(properties={
        'name': fields.TextField()
    })

    stock = fields.NestedField(properties={
        'amount': fields.IntegerField()
    })

    class Index:
        name = 'products'
        settings = {'number_of_shards': 1,
                    'number_of_replicas': 0}

    class Django:
        model = Product

        fields = [
            'name',
        ]
        related_models = [Brand, Stock]

    def get_queryset(self):
        return super(ProductDocument, self).get_queryset().prefetch_related('stock').select_related('brand')

    def get_instances_from_related(self, related_instance):

        if isinstance(related_instance, Brand):
            return related_instance.product_set.all()
        elif isinstance(related_instance, Stock):
            return related_instance.product

    @query_debugger
    def prepare(self, instance):
        return super(ProductDocument, self).prepare(instance)

    def get_indexing_queryset(self):
        return self.get_queryset()
